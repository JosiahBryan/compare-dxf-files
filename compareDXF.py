#def compareDXF (inputfile):
import sys
sys.path.append("/home/bryanjos")
sys.path.append("/home/bryanjos/.local/lib/python2.7/site-packages")
inputfile = sys.argv[1].split('/')[1]

import numpy as np
import dxfgrabber
import string
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
#import matplotlib.patches as mpatches
import mpld3#, json

# Read in DXF file solution
import os.path
if os.path.isfile("../../../home/bryanjos/dxfsols/" + inputfile):
  dxf = dxfgrabber.readfile("../../../home/bryanjos/dxfsols/" + inputfile)
else:
  sys.exit("No solution found for this filename.")
#print("DXF version: {}".format(dxf.dxfversion))
header_var_count = len(dxf.header) # dict of dxf header vars
layer_count = len(dxf.layers) # collection of layer definitions
block_definition_count = len(dxf.blocks) #  dict like collection of block definitions
entity_count = len(dxf.entities) # list like collection of entities

c_Line = [entity for entity in dxf.entities if entity.dxftype == 'LINE']
c_Cir = [entity for entity in dxf.entities if entity.dxftype == 'CIRCLE']
c_Arc = [entity for entity in dxf.entities if entity.dxftype == 'ARC']
c_Ell = [entity for entity in dxf.entities if entity.dxftype == 'ELLIPSE']
c_View = [entity for entity in dxf.entities if entity.dxftype == 'VIEWPORT']
c_Blk = [entity for entity in dxf.entities if entity.dxftype == 'INSERT']
c_Lay = [layer for layer in dxf.layers]

# Read in DXF file(s) to evaluate
dxf2 = dxfgrabber.readfile("../../../home/bryanjos/www/uploads/" + inputfile)
#print("DXF version: {}".format(dxf2.dxfversion))
header_var_count2 = len(dxf2.header) # dict of dxf header vars
last_saved_by = dxf2.header["$LASTSAVEDBY"]
layer_count2 = len(dxf2.layers) # collection of layer definitions
block_definition_count2 = len(dxf2.blocks) #  dict like collection of block definitions
entity_count2 = len(dxf2.entities) # list like collection of entities

c_Line2 = [entity for entity in dxf2.entities if entity.dxftype == 'LINE']
c_Cir2 = [entity for entity in dxf2.entities if entity.dxftype == 'CIRCLE']
c_Arc2 = [entity for entity in dxf2.entities if entity.dxftype == 'ARC']
c_Ell2 = [entity for entity in dxf2.entities if entity.dxftype == 'ELLIPSE']
c_View2 = [entity for entity in dxf2.entities if entity.dxftype == 'VIEWPORT']
c_Blk2 = [entity for entity in dxf2.entities if entity.dxftype == 'INSERT']
c_Lay2 = [layer for layer in dxf2.layers]

# Call f_LectDxf to read in DXF data
#[filename, pathname] = uigetfile({'*.dxf'},'Open Solution File','F:\Engineering Graphics\Enginr 1100\Exams\','Multiselect','off');
#[c_Line,c_Poly,c_Cir,c_Arc,c_Poi,c_Ell,c_View,c_Lay,c_Blk] = f_LectDxf([pathname filename]);

# Select all files to grade (DXF files...perhaps from AutoScript batch conversion?)
#[filename2, pathname2] = uigetfile({'*.dxf'},'Open Multiple Test Files','F:\Engineering Graphics\Enginr 1100\Exams\','Multiselect','on');
# Select folder to save Feedback Plots to (or use default)
#[dirname] = uigetdir('F:\Engineering Graphics\Enginr 1100\Exams\','Select Folder to Save Feedback Plots to');

# Convert filename2 to cell array if not already
# if !iscell(filename2)
#   filename2 = mat2cell(filename2,1);
# end
# Create blank cell matrix for recording PawPrints and grades
# CSVmatrix = cell(length(filename2),3);
# Cycle through all files, grading each one
#for bigloop = 1:length(filename2)
  # Call f_LectDxf to read in DXF data
  #[c_Line2,c_Poly2,c_Cir2,c_Arc2,c_Poi2,c_Ell2,c_View2,c_Lay2,c_Blk2] = f_LectDxf([pathname2 cell2mat(filename2(bigloop))]);
	
# Initialize grade tally starting at 100
grade = 100
# Initialize blank COMMENT string
COMMENT = ''

# COMPARE FILES
# Compare layer linetypes (assume file 1 is solution)
num_lays = len(c_Lay)
num_lays2 = len(c_Lay2)
# Create logic array [file1 lay1 == file2 lays; file1 lay2 == file2 lays; ...]
c_LayTrue = np.zeros((num_lays,num_lays2))
if (not num_lays == 0 and not num_lays2 == 0):
	for i in range(0,num_lays):
		for ii in range(0,num_lays2):
			c_LayTrue[i,ii] = c_Lay[i].linetype in c_Lay2[ii].linetype # find which layers in File1 share linetypes with File2
# Find which, if any, layer linetypes are missing
c_LayMissingIndex = np.where(c_LayTrue.sum(axis=1) == 0)[0]
layCOMMENT = ""
if len(c_LayMissingIndex) == 0:
	print("All solution layer linetypes were found in your file.")
else:
	print("The following solution layer linetypes were not found in your file:")
	for i in c_LayMissingIndex:
		print(c_Lay[i].linetype)
		layCOMMENT = layCOMMENT + c_Lay[i].linetype
	# Reduce grade by number of missing layer linetypes
	grade = grade - len(c_LayMissingIndex)
	COMMENT = COMMENT + "Missing linetypes:" + layCOMMENT + ".\n"
		
# Compare circles (assume file 1 is solution)
num_circles = len(c_Cir)
num_circles2 = len(c_Cir2)
# Create logic array [file1 circle1 == file2 circles; file1 circle2 == file2 circles; ...]
c_CirTrue = np.zeros((num_circles,num_circles2))
if (not num_circles == 0 and not num_circles2 == 0):
	for i in range(0,num_circles):
		for ii in range(0,num_circles2):
			if (np.max(np.abs(np.array(c_Cir[i].center) - np.array(c_Cir2[ii].center))) < 1e-4) and (np.abs(c_Cir[i].radius - c_Cir2[ii].radius) < 1e-4):# max to find if all three coordinates AND radius match
				# Once a possible match is found, then linetype must also be checked.
				if (dxf.layers[c_Cir[i].layer].linetype == dxf2.layers[c_Cir2[ii].layer].linetype):
					c_CirTrue[i,ii] = True
				# Otherwise check if layer linetype already missing (then don't count off for this)
				else:
					for iii in c_LayMissingIndex:
						if c_Lay[iii].linetype == dxf2.layers[c_Cir2[ii].layer].linetype:
							c_CirTrue[i,ii] = True
							break
# Find which, if any, circles are missing
c_CirMissingIndex = np.where(c_CirTrue.sum(axis=1) == 0)[0]
#cirCOMMENT = "";
if len(c_CirMissingIndex) == 0:
	print("All solution circles were found in your file.")
else:
	# print("The following solution circles were not found in File 2 (or had the wrong linetype):")
	# print("X		Y			Radius")
	# for i in c_CirMissingIndex:
		# print("%.8f" % c_Cir[i].center[0] + "	%.8f" % c_Cir[i].center[1] + "		%.8f" % c_Cir[i].radius)
		#cirCOMMENT = cirCOMMENT + "%.8f" % c_Cir[i].center[0] + "	%.8f" % c_Cir[i].center[1] + "		%.8f" % c_Cir[i].radius
	print(str(len(c_CirMissingIndex)) + " solution circles were not found in your file (or had the wrong linetype).")
	# Reduce grade by number of missing circles
	grade = grade - len(c_CirMissingIndex)
	#COMMENT = COMMENT + "Missing circles:" + cirCOMMENT + "."
						
# Compare arcs (assume file 1 is solution)
num_arcs = len(c_Arc)
num_arcs2 = len(c_Arc2)
# Create logic array [file1 arc1 == file2 arcs; file1 arc2 == file2 arcs; ...]
c_ArcTrue = np.zeros((num_arcs,num_arcs2))
if (not num_arcs == 0 and not num_arcs2 == 0):
	for i in range(0,num_arcs):
		for ii in range(0,num_arcs2):
			if (np.max(np.abs(np.array(c_Arc[i].center) - np.array(c_Arc2[ii].center))) < 1e-4) and (np.abs(c_Arc[i].radius - c_Arc2[ii].radius) < 1e-4):# max to find if all three coordinates AND radius match
				# Must also check start and end angles
				if (np.abs(round(c_Arc[i].start_angle,8) % 360 - round(c_Arc2[ii].start_angle,8) % 360) < 1e-4) and (np.abs(round(c_Arc[i].end_angle,8) % 360 - round(c_Arc2[ii].end_angle,8) % 360) < 1e-4): # modulo makes 0 and 360 equivalent, leaves anything between the same
					# Once a possible match is found, then linetype must also be checked.
					if (dxf.layers[c_Arc[i].layer].linetype == dxf2.layers[c_Arc2[ii].layer].linetype):
						c_ArcTrue[i,ii] = True
					# Otherwise check if layer linetype already missing (then don't count off for this)
					else:
						for iii in c_LayMissingIndex:
							if c_Lay[iii].linetype == dxf2.layers[c_Arc2[ii].layer].linetype:
								c_ArcTrue[i,ii] = True
								break
# Find which, if any, arcs are missing
c_ArcMissingIndex = np.where(c_ArcTrue.sum(axis=1) == 0)[0]
#arcCOMMENT = "";
if len(c_ArcMissingIndex) == 0:
	print("All solution arcs were found in your file.")
else:
	# print("The following solution arcs were not found in File 2 (or had the wrong linetype):")
	# print("X		Y			Radius")
	# for i in c_ArcMissingIndex:
		# print("%.8f" % c_Arc[i].center[0] + "	%.8f" % c_Arc[i].center[1] + "		%.8f" % c_Arc[i].radius + "		%.8f" % c_Arc[i].start_angle + "	%.8f" % c_Arc[i].end_angle)
		#arcCOMMENT = arcCOMMENT + "%.8f" % c_Arc[i].center[0] + "	%.8f" % c_Arc[i].center[1] + "		%.8f" % c_Arc[i].radius + "		%.8f" % c_Arc[i].start_angle + "	%.8f" % c_Arc[i].end_angle
	print(str(len(c_ArcMissingIndex)) + " solution arcs were not found in your file (or had the wrong linetype).")
	# Reduce grade by number of missing arcs
	grade = grade - len(c_ArcMissingIndex)
	#COMMENT = COMMENT + "Missing arcs:" + arcCOMMENT + "."

# Compare ellipses (assume file 1 is solution)
num_ellipses = len(c_Ell)
num_ellipses2 = len(c_Ell2)
# Create logic array [file1 ellipse1 == file2 ellipses; file1 ellipse2 == file2 ellipses; ...]
c_EllTrue = np.zeros((num_ellipses,num_ellipses2))
if (not num_ellipses == 0 and not num_ellipses2 == 0):
	for i in range(0,num_ellipses):
		for ii in range(0,num_ellipses2):
			if (np.max(np.abs(np.array(c_Ell[i].center) - np.array(c_Ell2[ii].center))) < 1e-4) and (np.max(np.abs(np.abs(np.array(c_Ell[i].major_axis)) - np.abs(np.array(c_Ell2[ii].major_axis)))) < 1e-4) and (np.abs(c_Ell[i].ratio - c_Ell2[ii].ratio) < 1e-4):# max to find if all three center coordinates AND major axis coordinates AND ratio match
				# ALSO uses absolute values of MajorAxisEndPoints in case major axis is given with negative sign
				# WARNING:  Ellipses that have been mirrored may be indistinguishable from those that have not.  AutoCAD plots them in opposite direction (theta increases CW instead of CCW), though their parameters may be identical to those of an unmirrored ellipse.
				# Must also check start and end angles
				if ((np.abs(c_Ell[i].start_param % (2*np.pi) - c_Ell2[ii].start_param % (2*np.pi)) < 1e-4) and (np.abs(c_Ell[i].end_param % (2*np.pi) - c_Ell2[ii].end_param % (2*np.pi)) < 1e-4)) or ((np.abs(c_Ell[i].end_param % (2*np.pi) - c_Ell2[ii].start_param % (2*np.pi)) < 1e-4) and (np.abs(c_Ell[i].start_param % (2*np.pi) - c_Ell2[ii].end_param % (2*np.pi)) < 1e-4)): # modulo makes 0 and 2*pi equivalent, leaves anything between the same -- ALSO checks for flipped start and end parameters (e.g., mirrored ellipse)
					# Once a possible match is found, then linetype must also be checked.
					if (dxf.layers[c_Ell[i].layer].linetype == dxf2.layers[c_Ell2[ii].layer].linetype):
						c_EllTrue[i,ii] = True
					# Otherwise check if layer linetype already missing (then don't count off for this)
					else:
						for iii in c_LayMissingIndex:
							if c_Lay[iii].linetype == dxf2.layers[c_Ell2[ii].layer].linetype:
								c_EllTrue[i,ii] = True
								break
# Find which, if any, ellipses are missing
c_EllMissingIndex = np.where(c_EllTrue.sum(axis=1) == 0)[0]
#ellCOMMENT = "";
if len(c_EllMissingIndex) == 0:
	print("All solution ellipses were found in your file.")
else:
	# print("The following solution ellipses were not found in File 2 (or had the wrong linetype):")
	# print("X	   Y	   MajorEndX	   MajorEndY	   Minor/Major Ratio	   StartAngle	   EndAngle")
	# for i in c_EllMissingIndex:
		# print("%.8f" % c_Ell[i].center[0] + "	%.8f" % c_Ell[i].center[1] + "	%.8f" % c_Ell[i].major_axis[0] + "	%.8f" % c_Ell[i].major_axis[1] + "	%.8f" % c_Ell[i].ratio + "	%.8f" % c_Ell[i].start_param + "	%.8f" % c_Ell[i].end_param)
		#ellCOMMENT = ellCOMMENT + "%.8f" % c_Ell[i].center[0] + "	%.8f" % c_Ell[i].center[1] + "		%.8f" % c_Ell[i].major_axis[0] + "		%.8f" % c_Ell[i].major_axis[1] + "	%.8f" % c_Ell[i].ratio + "	%.8f" % c_Ell[i].start_param + "	%.8f" % c_Ell[i].end_param
	print(str(len(c_EllMissingIndex)) + " solution ellipses were not found in your file (or had the wrong linetype).")
	# Reduce grade by number of missing ellipses
	grade = grade - len(c_EllMissingIndex)
	#COMMENT = COMMENT + "Missing ellipses:" + ellCOMMENT + "."

# Compare viewports (assume file 1 is solution)
num_views = len(c_View)
num_views2 = len(c_View2)
# Create logic array [file1 view1 == file2 views; file1 view2 == file2 views; ...]
c_ViewTrue = np.zeros((num_views,num_views2))
if (not num_views == 0 and not num_views2 == 0):
	for i in range(0,num_views):
		for ii in range(0,num_views2):
			if (np.abs((c_View[i].viewheight / c_View[i].height) - (c_View2[ii].viewheight / c_View2[ii].height)) < 1e-4):# check if scale factors are equal
				c_ViewTrue[i,ii] = True
# Find which, if any, scale factors are missing
c_ViewMissingIndex = np.where(c_ViewTrue.sum(axis=1) == 0)[0]
viewCOMMENT = ""
if len(c_ViewMissingIndex) == 0:
	print("All solution views were found in your file.")
else:
	print("The following solution viewport scale factors were not found in your file:")
	#print("ScaleFactor")
	for i in c_ViewMissingIndex:
		print("%.8f" % (c_View[i].viewheight / c_View[i].height))
		viewCOMMENT = viewCOMMENT + "%.8f" % (c_View[i].viewheight / c_View[i].height)
	# Reduce grade by number of missing views
	grade = grade - len(c_ViewMissingIndex)
	COMMENT = COMMENT + "Missing viewport scale factors:" + viewCOMMENT + "."

# Compare lines (assume file 1 is solution)
num_lines = len(c_Line)
num_lines2 = len(c_Line2)
num_blks2 = len(c_Blk2)
# Convert line data to matrix data for both files (for easier computation)
c_Line_mat = np.zeros((num_lines,6))
c_Line_mat_notitle_index = np.zeros([1,0], dtype=np.int)
for i in range(0,num_lines):
	c_Line_mat[i] = np.hstack(([c_Line[i].start,c_Line[i].end]))
	if not dxf.layers[c_Line[i].layer].name.upper() == "TITLE": # only consider solution lines that are not part of the title block
		c_Line_mat_notitle_index = np.hstack((c_Line_mat_notitle_index,np.array([[i]]))) # array of indices of non-title-block lines
num_lines_notitle = c_Line_mat_notitle_index.shape[1] # number of solution lines not in the title block (these are the only ones to be checked)
#		c_Line_mat(i,:) = round(1e8*cell2mat(c_Line(i,1)))/1e8;  # round to 8th decimal place, but no decimal parameter in Octave
#	else
#		num_lines = num_lines - 1; # Remove title block from solution line count
#		tblock_lines = [tblock_lines i]; # Build list of lines to remove
#		#c_Line_mat(end,:) = [];
c_Line2_mat = np.zeros((num_lines2,6))
c_Line2_mat_nopaper_index = np.zeros([1,0], dtype=np.int)
for i in range(0,num_lines2):
	c_Line2_mat[i] = np.hstack([c_Line2[i].start,c_Line2[i].end])
	# Also check test lines for being in PAPERSPACE.  These lines will be checked against but will not be plotted.
	if not c_Line2[i].paperspace == True: # only add test lines that are NOT in paperspace
		c_Line2_mat_nopaper_index = np.hstack((c_Line2_mat_nopaper_index,np.array([[i]]))) # array of indices of non-paperspace lines
num_lines2_nopaper = c_Line2_mat_nopaper_index.shape[1] # number of test lines not in paperspace (these are the only ones to be plotted)
#for i = 1:num_lines2
#if ~strcmp(char(c_Line2(i,2)),'Title')
#  c_Line2_mat(i,:) = round(1e8*cell2mat(c_Line2(i,1)))/1e8;  # round to 8th decimal place, but no decimal parameter in Octave
#else
  #num_lines2 = num_lines2 - 1;
  #c_Line2_mat(end,:) = [];
#end
# Create logic array [file1 line1 == file2 lines; file1 line2 == file2 lines; ...]
c_LineTrue = np.zeros((num_lines_notitle,num_lines2))
if (not num_lines_notitle == 0 and not num_lines2 == 0) or (not num_lines_notitle == 0 and not num_blks2 == 0): # if solution & test have lines, or if solution has lines & test has blocks (e.g., centermark)
	for i in c_Line_mat_notitle_index[0]: # only consider lines that are not in the Title layer
		for ii in range(0,num_lines2):
			if (np.max(np.abs(c_Line_mat[i] - c_Line2_mat[ii])) < 1e-4) or ((np.max(np.abs(c_Line_mat[i,3:6] - c_Line2_mat[ii,0:3])) < 1e-4) and (np.max(np.abs(c_Line_mat[i,0:3] - c_Line2_mat[ii,3:6])) < 1e-4)):# max to find if all three coordinates of start AND end match, OR if line goes opposite direction from solution
				# Once a possible match is found, then linetype must also be checked.  (Keep indices same for c_Line and c_Line_mat to avoid issues here!)
				if (dxf.layers[c_Line[i].layer].linetype == dxf2.layers[c_Line2[ii].layer].linetype):
					c_LineTrue[i,ii] = True
				# Otherwise check if layer linetype already missing (then don't count off for this)
				else:
					for iii in c_LayMissingIndex:
						if c_Lay[iii].linetype == dxf2.layers[c_Line2[ii].layer].linetype:
							c_LineTrue[i,ii] = True
							break
		if (not np.max(c_LineTrue[i])) or (num_lines2 == 0 and not num_blks2 == 0): # if not an exact match, or if test has no lines but has blocks (e.g., centermark)
			# NOTE:  ***MUST ALSO LOOK FOR ANY LINE SUMS THAT EQUAL THE SOLUTION LINE***
			# CHECK for chain of (x,y)'s through any possible direction; CHECK slopes
			
			# ALSO CHECK centerlines by checking for lines of correct slope through centerpoint on CENTER layer
			# IF solution line has CENTER2 linetype
			if dxf.layers[c_Line[i].layer].linetype == "CENTER2":
				# THEN find slope and midpoint of solution line
				#	First check for zero in denominator
				if np.abs(c_Line_mat[i,3] - c_Line_mat[i,0]) < 1e-4: #
					slope1 = np.inf
				else:
					slope1 = (c_Line_mat[i,4] - c_Line_mat[i,1]) / (c_Line_mat[i,3] - c_Line_mat[i,0])
				midpt1 = np.array(([(c_Line_mat[i,0] + c_Line_mat[i,3]) / 2, (c_Line_mat[i,1] + c_Line_mat[i,4]) / 2]))	
				# Correct for -Inf possibilities
				#if slope1 == -Inf
				#slope1 = Inf;            
				
				# CHECK for block with basepoint lying on solution centerline (i.e., if 2017 CENTERMARK command)
				for ii in range(0,num_blks2):
					#basept = round(1e8*cell2mat(c_Blk2(ii,1)))/1e8;  # round to 8th decimal place, but no decimal parameter in Octave
					basept = np.array(c_Blk2[ii].insert[0:2]) # only use X and Y coordinates
					
					# Find slope of block basepoint relative to solution midpoint (then check if equal to solution slope)
					#	First check for zero in denominator
					if np.abs(basept[0] - midpt1[0]) < 1e-4:
						slopebasept_midpt = np.inf
					else:
						slopebasept_midpt = (basept[1] - midpt1[1]) / (basept[0] - midpt1[0])
					#if slopebasept_midpt == -Inf # correct for -Inf possibility
					#  slopebasept_midpt = Inf;
					
					if (slope1 == slopebasept_midpt or np.abs(slope1 - slopebasept_midpt) < 1e-4 or np.max(np.abs(basept-midpt1)) < 1e-4): # if slope equals solution slope OR if basept and midpt identical
						# THEN check if test basept lies between endpoints of solution line
						x1min = np.min([c_Line_mat[i,0], c_Line_mat[i,3]])
						x1max = np.max([c_Line_mat[i,0], c_Line_mat[i,3]])
						y1min = np.min([c_Line_mat[i,1], c_Line_mat[i,4]])
						y1max = np.max([c_Line_mat[i,1], c_Line_mat[i,4]])
						if (round(basept[0],8) >= round(x1min,8) and round(basept[0],8) <= round(x1max,8) and round(basept[1],8) >= round(y1min,8) and round(basept[1],8) <= round(y1max,8)):
							#if isempty(c_LineTrueForward) # i.e., if no test lines exist and no block matches already found
							#if size(c_LineTrueForward,2) == num_lines2 # i.e., if no block matches already found
							#  c_LineTrueForward = [c_LineTrueForward zeros(num_lines,1)]; # then add a column to record block matches
							#  c_LineTrueBackward = [c_LineTrueBackward zeros(num_lines,1)]; 
							#end
											
							# Once a possible match is found, then linetype must also be checked.  (Keep indices same for c_Line and c_Line_mat to avoid issues here!)
							if (dxf.layers[c_Line[i].layer].linetype == dxf2.layers[c_Blk2[ii].layer].linetype):
								c_LineTrue[i,1] = True # use column 1 because # columns may differ
							# Otherwise check if layer linetype already missing (then don't count off for this)
							else:
								for iii in c_LayMissingIndex:
									if c_Lay[iii].linetype == dxf2.layers[c_Blk2[ii].layer].linetype:
										c_LineTrue[i,1] = True # use column 1 because # columns may differ
										break
												
						# CHECK if linetype already missing?  IF this block matches solution linetype OR solution linetype already missing from file
						#if strcmp(c_Line[i,3],c_Blk2[ii,3]) || max(strcmp(c_Line(i,3),cell2mat(c_Lay(c_LayMissingIndex,1))))
							# THEN consider this block to be a matching line
						#	c_LineTrueForward(i,1) = 2; # set first (arbitrary) test line to 2 (flag for block match)
				
				# Find slope of each test line AND of a line passing through midpoint1 and an endpoint from test line (either endpoint should work)
				for ii in range(0,num_lines2):
					#	First check for zero in denominator
					if (c_Line2_mat[ii,3] - c_Line2_mat[ii,0]) < 1e-4:
						slope2 = np.inf
					else:
						slope2 = (c_Line2_mat[ii,4] - c_Line2_mat[ii,1]) / (c_Line2_mat[ii,3] - c_Line2_mat[ii,0])
										
					#	First check for zero in denominator
					if (midpt1[0] - c_Line2_mat[ii,0]) < 1e-4:
						slope12 = np.inf
					else:
						slope12 = (midpt1[1] - c_Line2_mat[ii,1]) / (midpt1[0] - c_Line2_mat[ii,0])
			
					# Correct for -Inf possibilities
					#if slope2 == -Inf
					#  slope2 = Inf;
				
					#if slope12 == -Inf
					#  slope12 = Inf;            
			
					# Check for equality of slopes of solution line, test line, AND line passing through midpoint1 and endpoint of test line
					if ((slope1 == slope2 or np.abs(slope1 - slope2) < 1e-4) and (slope1 == slope12 or np.abs(slope1 - slope12) < 1e-4)):
						# THEN check if midpoint1 lies between endpoints of test line
						x2min = np.min([c_Line2_mat[ii,0], c_Line2_mat[ii,3]])
						x2max = np.max([c_Line2_mat[ii,0], c_Line2_mat[ii,3]])
						y2min = np.min([c_Line2_mat[ii,1], c_Line2_mat[ii,4]])
						y2max = np.max([c_Line2_mat[ii,1], c_Line2_mat[ii,4]])
						if (midpt1[0] >= x2min and midpt1[0] <= x2max and midpt1[1] >= y2min and midpt1[1] <= y2max):
						# Once a possible match is found, then linetype must also be checked.  (Keep indices same for c_Line and c_Line_mat to avoid issues here!)
							if (dxf.layers[c_Line[i].layer].linetype == dxf2.layers[c_Line2[ii].layer].linetype):
								# THEN consider this to be a matching line
								c_LineTrue[i,ii] = True
							# Otherwise check if layer linetype already missing (then don't count off for this)
							else:
								for iii in c_LayMissingIndex:
									if c_Lay[iii].linetype == dxf2.layers[c_Line2[ii].layer].linetype:
										c_LineTrue[i,ii] = True
										break
			
	  # Once a possible match is found, then linetype must also be checked
	  #for ii = find(c_LineTrueForward(i,:) | c_LineTrueBackward(i,:)) # find possible matches
		# CHECK if linetype already missing?  IF test linetype does not match solution AND if solution linetype not already missing from file 
		#if ~strcmp(c_Line(i,3),c_Line2(ii,3)) && ~max(strcmp(c_Line(i,3),cell2mat(c_Lay(c_LayMissingIndex,1))))
		#  if c_LineTrueForward(i,ii) <= 1 # Also check that this is not a block match (not flag value of 2)
		#	c_LineTrueForward(i,ii) = 0; # then set match values to false
		#	c_LineTrueBackward(i,ii) = 0;
	
# c_LineTrue = c_LineTrueForward | c_LineTrueBackward;
# Find which, if any, lines are missing
# NOTE:  WILL NEED TO REMOVE ALL ROWS WITH TITLE-BLOCK LINE INDICES FIRST!!!!!!!!!!!!!!!!!!!!
c_LineMissingIndex = np.where(c_LineTrue[c_Line_mat_notitle_index,:].sum(axis=2) == 0)[1] # only considers lines NOT in title layer
if len(c_LineMissingIndex) == 0:
	print("All solution lines were found in your file.")
else:
	#print("The following solution lines were not found in File 2 (or had the wrong layer linetype):")
	#print("(NOTE:  Centerlines may be considered missing if they have the wrong slope or do not pass through the center.)")
	#print("X1		Y1		Z1		X2		Y2		Z2		LAYER")
	#for i in c_LineMissingIndex: # only considers lines NOT in title layer
	#	print("%.8f" % c_Line_mat[c_Line_mat_notitle_index,:][0][i][0] + "	%.8f" % c_Line_mat[c_Line_mat_notitle_index,:][0][i][1] + "	%.8f" % c_Line_mat[c_Line_mat_notitle_index,:][0][i][2] + "	%.8f" % c_Line_mat[c_Line_mat_notitle_index,:][0][i][3] + "	%.8f" % c_Line_mat[c_Line_mat_notitle_index,:][0][i][4] + "	%.8f" % c_Line_mat[c_Line_mat_notitle_index,:][0][i][5] + "	" + c_Line[c_Line_mat_notitle_index[0][i]].layer)
	print(str(len(c_LineMissingIndex)) + " solution lines were not found in your file (or had the wrong layer linetype).")
	# Reduce grade by number of missing lines
	grade = grade - len(c_LineMissingIndex)

	

#c_Line_mat(c_LineMissingIndex,:)
#c_Line(c_LineMissingIndex,2)
# Reduce grade by number of missing lines
#grade = grade - length(c_LineMissingIndex);
# Add comment to COMMENT string
#lineCOMMENT = repmat(' ', [1 100]);
#lineCOMMENT = "";
#for i = 1:size(c_LineMissingIndex,1)
#  lineCOMMENT = [lineCOMMENT sprintf('#5f \t #5f \t #5f \t #5f \t #5f \t #5f \t  ',c_Line_mat(c_LineMissingIndex(i),:)),cell2mat(c_Line(c_LineMissingIndex(i),2)),"\n"];
#end
#COMMENT = [COMMENT "The following solution lines were not found in your file (or had the wrong layer linetype):\n(NOTE:  Centerlines may be considered missing if they have the wrong slope or do not pass through the center.)\nX1 \t \t Y1 \t \t Z1 \t \t X2 \t \t Y2 \t \t Z2 \t  LAYER\n" lineCOMMENT];


#  PLACE IN SINGLE EXCEL CELL BY PLACING DOUBLE QUOTES BEFORE AND AFTER
# Finish comment array with double quote
#COMMENT = [COMMENT '"'];


# FIGURES
#if plot_figure == true
#figure
# PRINT TEST IN ONE LINETYPE AND MISSING SOLUTION IN ANOTHER
fig, ax = plt.subplots(subplot_kw=dict(facecolor='#EEEEEE'))
fig.set_size_inches(9,9)
ax.grid(color='#646464', linestyle='dashed')
ax.set_title("Below is your drawing. \nRed highlighted features are inaccurate (e.g., location, length, linetype) or missing from your file.", size=12)

#labels = ['point {0}'.format(i + 1) for i in range(N)]
#tooltip = mpld3.plugins.PointLabelTooltip(scatter, labels=labels)
#mpld3.plugins.connect(fig, tooltip)

mpld3.plugins.connect(fig, mpld3.plugins.Zoom(True,True))
#mpld3.plugins.MousePosition(12,'.3g')
#print( mpld3.save_html(fig)

line2 = 0
for i in range(0,num_lines2_nopaper):
	line1, = ax.plot(c_Line2_mat[c_Line2_mat_nopaper_index[0][i]][[0,3]], c_Line2_mat[c_Line2_mat_nopaper_index[0][i]][[1,4]], 'b-')
for i in c_LineMissingIndex:
	line2, = ax.plot(c_Line_mat[c_Line_mat_notitle_index[0][i]][[0,3]], c_Line_mat[c_Line_mat_notitle_index[0][i]][[1,4]], 'r-', linewidth=3)
#plot(c_Line_mat(c_LineMissingIndex,[1 4]),c_Line_mat(c_LineMissingIndex,[2 5])','r','LineWidth',3)

# arcs
for i in range(0,num_arcs2):
  startangle = c_Arc2[i].start_angle
  endangle = c_Arc2[i].end_angle
  if endangle < startangle:
    endangle = endangle + 360 # keeps arcs going in the right direction
  t = np.linspace(startangle,endangle,360)*np.pi/180
  x = c_Arc2[i].center[0] + c_Arc2[i].radius*np.cos(t)
  y = c_Arc2[i].center[1] + c_Arc2[i].radius*np.sin(t)
  line1, = ax.plot(x,y,'b')
for i in c_ArcMissingIndex:
  startangle = c_Arc[i].start_angle
  endangle = c_Arc[i].end_angle
  if endangle < startangle:
    endangle = endangle + 360 # keeps arcs going in the right direction
  t = np.linspace(startangle,endangle,360)*np.pi/180
  x = c_Arc[i].center[0] + c_Arc[i].radius*np.cos(t)
  y = c_Arc[i].center[1] + c_Arc[i].radius*np.sin(t)
  line2, = ax.plot(x,y,'r',linewidth=3)

  # circles
for i in range(0,num_circles2):
  t = np.linspace(0,360,361)*np.pi/180
  x = c_Cir2[i].center[0] + c_Cir2[i].radius*np.cos(t)
  y = c_Cir2[i].center[1] + c_Cir2[i].radius*np.sin(t)
  line1, = ax.plot(x,y,'b')
for i in c_CirMissingIndex:
  t = np.linspace(0,360,361)*np.pi/180
  x = c_Cir[i].center[0] + c_Cir[i].radius*np.cos(t)
  y = c_Cir[i].center[1] + c_Cir[i].radius*np.sin(t)
  line2, = ax.plot(x,y,'r',linewidth=3)
  
# ellipses
for i in range(0,num_ellipses2):
  startangle = c_Ell2[i].start_param # in radians
  endangle = c_Ell2[i].end_param # in radians
  if endangle < startangle:
    endangle = endangle + 2*np.pi # keeps arcs going in the right direction
  # Specify orientation of major axis
  if c_Ell2[i].major_axis[0] == 0: # avoid divide-by-zero error (if y-component of major axis is zero, then set Ell_angle = 0)
    if c_Ell2[i].major_axis[1] > 0:
      Ell_angle = np.pi/2
    elif c_Ell2[i].major_axis[1] < 0:
      Ell_angle = -np.pi/2
    else:
      Ell_angle = 0
  else:
    Ell_angle = np.arctan2(c_Ell2[i].major_axis[1],c_Ell2[i].major_axis[0]) #np.arctan(c_Ell2[i].major_axis[1]/c_Ell2[i].major_axis[0]) # specifies orientation of major axis
  Ell_a = np.sqrt(c_Ell2[i].major_axis[0]**2+c_Ell2[i].major_axis[1]**2) # major axis length
  Ell_b = c_Ell2[i].ratio * Ell_a; # minor axis length
  t = np.linspace(startangle,endangle,360) # in radians
  x = c_Ell2[i].center[0] + Ell_a*np.cos(t)*np.cos(Ell_angle)-Ell_b*np.sin(t)*np.sin(Ell_angle)
  y = c_Ell2[i].center[1] + Ell_a*np.cos(t)*np.sin(Ell_angle)+Ell_b*np.sin(t)*np.cos(Ell_angle)
  line1, = ax.plot(x,y,'b')
for i in c_EllMissingIndex:
  startangle = c_Ell[i].start_param # in radians
  endangle = c_Ell[i].end_param # in radians
  if endangle < startangle:
    endangle = endangle + 2*np.pi # keeps arcs going in the right direction
  # Specify orientation of major axis
  if c_Ell[i].major_axis[0] == 0: # avoid divide-by-zero error (if y-component of major axis is zero, then set Ell_angle = 0)
    if c_Ell[i].major_axis[1] > 0:
      Ell_angle = np.pi/2
    elif c_Ell[i].major_axis[1] < 0:
      Ell_angle = -np.pi/2
    else:
      Ell_angle = 0
  else:
    Ell_angle = np.arctan2(c_Ell[i].major_axis[1],c_Ell[i].major_axis[0]) #np.arctan(c_Ell[i].major_axis[1]/c_Ell[i].major_axis[0]) # specifies orientation of major axis
  Ell_a = np.sqrt(c_Ell[i].major_axis[0]**2+c_Ell[i].major_axis[1]**2) # major axis length
  Ell_b = c_Ell[i].ratio * Ell_a; # minor axis length
  t = np.linspace(startangle,endangle,360) # in radians
  x = c_Ell[i].center[0] + Ell_a*np.cos(t)*np.cos(Ell_angle)-Ell_b*np.sin(t)*np.sin(Ell_angle)
  y = c_Ell[i].center[1] + Ell_a*np.cos(t)*np.sin(Ell_angle)+Ell_b*np.sin(t)*np.cos(Ell_angle)
  line2, = ax.plot(x,y,'r',linewidth=3)


if not line2 == 0:
  ax.legend([line1, line2], ['Your drawing', 'Missing from Your File'], loc='lower right')
else:
  ax.legend([line1], ['Your drawing'], loc='lower right')

  
  
  
  
  
#plt.figure(figsize=(6,6)) #axis image
ax.set_xlabel(COMMENT)
ax.set_aspect('equal', adjustable='datalim')

#printname = [dirname cell2mat(filename2(bigloop)) '.pdf']; # uses original filename
# OR generate random file name
#random_number = round(rand(1)*1e10);
#printname = [dirname '\' num2str(random_number) '.pdf'];

#print(printname) # save plot as PDF file
#close
#end

# Strip PawPrint from filename
#paw = strtok(cell2mat(filename2(bigloop)), '_.');
#paw = strsplit(cell2mat(filename2(bigloop)),{'_','.'},'CollapseDelimiters',true);

# Insert PawPrint and grade into matrix to be saved to CSV file
#CSVmatrix(bigloop,:) = {cell2mat(paw(end-1)), grade, COMMENT}
#CSVmatrix(bigloop,:) = {cell2mat(paw(end-1)), grade, random_number}


  # THINGS TO CHECK
  # LTS & PSLTS
  # PawPrint as "last saved by"?
  # Time markers - start with $TD
  

# Save LOG FILE to upload!
import csv
from time import localtime, strftime
fields=[strftime("%Y-%m-%d %H:%M:%S", localtime()), last_saved_by, dxf2.header["$TDUCREATE"], dxf2.header["$TDUPDATE"], inputfile, grade]
with open('../../../home/bryanjos/upload_log.csv', 'a') as f:
    writer = csv.writer(f)
    writer.writerow(fields)

fig.tight_layout()
#print(inputfile)
print(mpld3.fig_to_html(fig))
#print(mpld3.fig_to_html(fig).replace("\n","\r").replace("var figwidth = 8.0 * 80","var figwidth = $(window).width()*.9;").replace("var figheight = 6.0 * 80","var figheight = $(window).height()*.9;"))