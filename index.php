<?php 
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);
session_start();
?>

<html>
<head>
	<meta charsetr="utf-8">
	<title>Upload DXF File</title>
</head>

<body>

<form action="upload.php" method="post" enctype="multipart/form-data">
    Select DXF file to upload:
    <input type="file" name="fileToUpload" id="fileToUpload" accept=".dxf">
    <input type="submit" value="Upload DXF File" name="submit">
	<span class="error"><?php echo $_SESSION["message"];?></span>
</form>
<p>
- You <b>MUST</b> save your file as a DXF file before uploading it here.<br>- Make sure the filename is spelled <b>EXACTLY</b> as it is in the textbook.  Otherwise this site will not know which solution to compare your drawing to.<br>
- Be sure to follow <b>ALL</b> directions in the textbook exactly.<br>- If you do not start your drawing in the location specified by the book, this site may consider your entire drawing to be incorrect.<br>- If the book does not specify a starting location for the drawing, then assume the bottommost and leftmost points in the drawing (not counting centerlines) are on the x- and y-axes, respectively.<br>
- This site will only check basic geometric shapes, centerlines, linetypes, and viewport scales.<br>- It currently does not check for polylines, LIMITS, UNITS, LTSCALE, PSLTSCALE, HATCH, dimensioning, or other AutoCAD features (so it won't check "blank" files that have no saved geometry).<br>- If you're using Microsoft Edge as your browser, you may run into difficulties uploading files on this page.  Please try switching to Chrome or Firefox to see if the problem is fixed.
</p>
<!--<iframe name="formframe" style="display: none;">-->
<!--<script type="text/javascript" src="http://d3js.org/d3.v4.min.js"></script>-->
<!--<script type="text/javascript" src="http://mpld3.github.io/js/mpld3.v0.2.js"></script>-->
<script type="text/javascript" src="d3.v3.min.js"></script>
<script type="text/javascript" src="mpld3.v0.3.min.js"></script>
<div id="fig01"></div>
<?php if ($_SESSION["target_file"] != "") {$output = shell_exec("python ../../../home/bryanjos/compareDXF.py '" . $_SESSION["target_file"] . "' 2>&1"); echo "<div style='white-space: pre-wrap;'>". $output."</div>";} unlink($_SESSION["target_file"]);?><!-- $output = shell_exec("python -c 'print 100' 2>&1"); echo $output; echo "hi"; ?>-->
<!--<script type="text/javascript">
  var json01 = { <snip JSON code> };
  mpld3.draw_figure("fig01", json01);
</script>-->





<?php
// remove all session variables
session_unset();

// destroy the session
session_destroy();
?>

</body>
</html>
